module.exports = {
    // the main entry of our app
    entry: ['./src/index.js', './src/auth/index.js'],
    // output configuration
    output: {
        path: __dirname + '/build/',
        publicPath: 'build/',
        filename: 'build.js'
    },
    /*plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            "root.jQuery": "jquery"
        })
    ],
    resolve: {
        alias: {
            jquery: "jquery/dist/jquery"
        }
    },*/
    // how modules should be transformed
    module: {
        loaders: [
            // process *.vue files using vue-loader
            { test: /\.vue$/, loader: 'vue' },
            // process *.js files using babel-loader
            // the exclude pattern is important so that we don't
            // apply babel transform to all the dependencies!
            { test: /\.js$/, loader: 'babel', exclude: /node_modules/ }
        ]
    },
    // configure babel-loader.
    // this also applies to the JavaScript inside *.vue files
    babel: {
        presets: ['es2015'],
        plugins: ['transform-runtime']
    }
}
