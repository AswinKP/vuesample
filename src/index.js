import Vue from 'vue'
import App from './components/App.vue'
import Home from './components/Home.vue'
import GetSettings from './components/GetSettings.vue'
import Settings from './components/Settings.vue'
import Signup from './components/Signup.vue'
import Login from './components/Login.vue'
import IncidentTrendI7 from './components/IncidentTrendI7.vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
var jwtDecode = require('jwt-decode');
var $ = require('jquery')
Vue.use(VueResource)
Vue.use(VueRouter)
import auth from './auth'

Vue.http.headers.common['Authorization'] = 'jwt ' + localStorage.getItem('access_token');

export function config(){
  return {
  SITE_URL: "http://localhost:5000/",
  API_HOME : "http://localhost:5000/api/v1/"
  }
}

/*Vue.http.options.root = 'http://localhost:5000/api/v1'*/

// Check the user's auth status when the app starts
auth.checkAuth()

const routes = [
  { path: '/home', component: Home },
  { path: '/get-settings', component:GetSettings  },
  { path: '/login', component: Login },
  { path: '/signup', component: Signup },
  { path: '/settings', component: Settings },
  { path: '/i7', component: IncidentTrendI7 },
  { path: '*', redirect: '/home'}
]


const router = new VueRouter({
  routes // short for routes: routes
})

const app = new Vue({
  router
}).$mount('#app')

