import {router} from '../index'
import Vue from 'vue'
import {config} from '../index'

const LOGIN_URL = config().SITE_URL + 'auth'
const SIGNUP_URL = config().API_HOME + 'signup'

export default {

  user: {
    authenticated: false
  },

  login(context, creds, redirect) {
    context.$http.post(LOGIN_URL, creds, (data) => {
      localStorage.setItem('access_token', data.access_token);
      Vue.http.headers.common['Authorization'] = this.getAuthHeaderToSend();
      this.user.authenticated = true
      
      if(redirect) {
        router.go(redirect)        
      }

    }).error((err) => {
      context.error = err
    })
  },

  signup(context, creds, redirect) {
    context.$http.post(SIGNUP_URL, creds, (data) => {
      localStorage.setItem('access_token', data.access_token)

      this.user.authenticated = true
      console.log(getAuthHeaderToSend());
      Vue.http.headers.common['Authorization'] = this.getAuthHeaderToSend();
      if(redirect) {
        router.go(redirect)        
      }

    }).error((err) => {
      context.error = err
    })
  },

  logout() {
    localStorage.removeItem('access_token')
    this.user.authenticated = false
  },

  checkAuth() {
    var jwt = localStorage.getItem('access_token')
    if(jwt) {
      this.user.authenticated = true
    }
    else {
      this.user.authenticated = false      
    }
  },


  getAuthHeader() {
    return  localStorage.getItem('access_token')
  },

    getAuthHeaderToSend() {
    return  "JWT "+localStorage.getItem('access_token')
  }
}
